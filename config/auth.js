module.exports = {
  ensureAuthenticated: function(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    req.session.returnTo = '/users' + req.url;
    console.log('session return to',req.session.returnTo);
    req.flash('error_msg', 'Please create an account or log in.');
    res.redirect('/users/login');
  }
};
