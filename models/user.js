const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  accountRS:{
    type: String
  },
  publicKey: {
    type: String
  },
  encryptedData: {
    type: String
  },
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  confirmEmailToken: String,
  confirmEmailExpires: Date,
  airdrop: {
    type: Boolean,
    default: false
  },
  accountVerified: {
    type: Boolean,
    default: false
  }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
