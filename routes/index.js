const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');
const nodeMailer = require('nodemailer');
var request = require('request');
var crypto = require("crypto");
var MongoClient = require('mongodb').MongoClient;

// Welcome Page
router.get('/', (req, res) => res.render('home', {user: req.user, title: 'Birdlance | Home' }));

// Privacy Page
router.get('/privacy', (req, res) => res.render('privacy', {user: req.user, title: 'Birdlance | Privacy policy' }));

//contact form post
router.post('/contact-send', function (req, res) {
   // g-recaptcha-response is the key that browser will generate upon form submit.
  // if its blank or null means user has not selected the captcha, so return the error.
  if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
    req.flash(
      'error_msg',
      'Please complete captcha.'
      );
    return res.redirect('/#contact');
  }
  // Put your secret key here.
  var secretKey = process.env.google_reCAPTCHA;
  // req.connection.remoteAddress will provide IP address of connected user.
  var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
  // Hitting GET request to the URL, Google will respond with success or error scenario.
  request(verificationUrl,function(error,response,body) {
    body = JSON.parse(body);
    // Success will be true or false depending upon captcha validation.
    if(body.success !== undefined && !body.success) {
      req.flash(
        'error_msg',
        'captcha validation was invalid.'
        );
      return res.redirect('/#contact');
    }
    const sgMail = require('@sendgrid/mail');
    sgMail.setApiKey(process.env.SENDGRID_API_KEY) // Input Api key or add to environment config
    const message = { 
      to : 'info@birdlance.com',
      from :  { email : req.body.email, name: req.body.name },
      subject : req.body.subject,
     "content": [
          {
            "type": "text/html", 
            "value": req.body.message
          }
        ]}
      sgMail.send(message).then((sent) => {
        // Awesome Logic to check if mail was sent
      })
      .catch(error => {
  
          //Log friendly error
          console.error(error.toString());
      
          //Extract error msg
          const {message, code, response} = error;
      
          //Extract response msg
          const {headers, body} = response;
        });
    req.flash(
    'success_msg',
    'Your message has been sent.'
    );
    res.redirect('/#contact');
  });
});

// Photo single
router.get('/shot', (req, res) => res.render('shot', { layout: false }));

// Reverse image search
router.post("/search", function(req, res) {
  const  body = req.body;
  let errors = [];
  if (!body.url) {
    errors.push({ msg: 'Please enter a valid image URL' });
  }

  if (errors.length > 0) {
    res.render('home', {
      errors,
      user: req.user,
      title: 'Birdlance | Home'
    });
  } else {
  const filename = crypto.randomBytes(32).toString("hex");
    var options = {
    uri: 'https://birdlance-api-image-hash-inst.herokuapp.com/api_v1.0/calculate_instant',
    method: 'POST',
      json: {
        "identifier_id": filename,
        "identifier_type":"Birdlance-image-hashing API",
        "image": {
          "id":filename,
          "url":body.url
        }
      }
    }

  request(options, function (error, response, api_body) {
    if(error) {
      console.log(error);
    }
    if(response) {
      var data = JSON.parse(JSON.stringify(response))
      if(data) {
        var body = data.body
        if(body) {
          var hash = body.hash
          if(hash)
          {
            MongoClient.connect(process.env.MongoURI2,{ useNewUrlParser: true }, function (err, db) {  
              if(err) throw err
              var dbo = db.db("birdlance");
              var myquery = { phash: hash };      
              dbo.collection("blnc_imagehash").find(myquery).toArray(function(err, results) {
                  if (err) throw err;
                  db.close();
                  res.render('search-results', { user: req.user, results, title:'Birdlance | Search results' });
                });
            });
        }
        }
      }
    }
  });
}
});

module.exports = router;
