const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const { ensureAuthenticated } = require('../config/auth');
var async = require("async");
var crypto = require("crypto");
const Cryptr = require('cryptr');
const cryptr = new Cryptr('process.env.cryptr');
var request = require('request');
const multer = require('multer');
const ipfsAPI = require('ipfs-api');
const ipfs = ipfsAPI('ipfs.infura.io', '5001', {protocol: 'https'});
const path = require('path');
const DOCUMENT = require("../models/Document");
const fs = require('fs')
const sharp = require('sharp')
var ExifImage = require('exif').ExifImage;
var MongoClient = require('mongodb').MongoClient;


// Multer ships with storage engines DiskStorage and MemoryStorage
// And Multer adds a body object and a file or files object to the request object. The body object contains the values of the text fields of the form, the file or files object contains the files uploaded via the form.
var storage =  multer.diskStorage({destination:path.resolve(__dirname, ".","uploads")});
var upload = multer({ storage: storage });

// Load User model
const User = require('../models/user');

// Login Page
router.get('/login', (req, res) => res.render('login',{ user: req.user,  title:'Birdlance | Login'}));

// Register Page
router.get('/register', (req, res) => {
  res.render('register',{ user: req.user, title:'Birdlance | Register'})
});

// account verify Page
router.get('/account-verify', (req, res) => res.render('account-verify',{ user: req.user,  title:'Birdlance | Verify account'}));

// Account Page
router.get('/account', ensureAuthenticated, (req, res) => {
  var user = JSON.parse(JSON.stringify(req.user));
  var balance = 0;
    if (user.accountRS) {
    var request = require('request');
    request('https://wallet.birdlance.com:9085/nxt?requestType=getAccount&account=' + user.accountRS, function (error, response, body) {
      console.log(body);
      var result = JSON.parse(body);
      balance = result.balanceNQT / 100000000
      res.render('account', { user: req.user, balance: balance, title:'Birdlance | My account' });
    });  
  }
  else {
  res.render('account', { user: req.user, balance: balance, title:'Birdlance | My account' });
  }
});

// Witdraw Page
router.get('/account-withdraw', ensureAuthenticated, (req, res) => {
  res.render('account-withdraw', { user: req.user, title:'Birdlance | Withdraw balance' });
});

// account settings Page
router.get('/account-settings', ensureAuthenticated, (req, res) => {
  res.render('account-settings', { user: req.user, title:'Birdlance | update password' });
});

// Reset-password page
router.get('/reset-password', (req, res) => {
  res.render('reset-password', { user: req.user, title:'Birdlance | Reset password' });
});

// upload Page
router.get('/upload-photos', ensureAuthenticated, (req, res) => {
  res.render('upload-photos', { user: req.user, title:'Birdlance | Upload photos' });
});

// my photos Page
router.get('/my-photos', ensureAuthenticated, (req, res) => {
  DOCUMENT.find(
    {created_by: req.user.email},
    (err, photos) => {
      if (err) {
        return next(err);
      }
      MongoClient.connect(process.env.MongoURI2,{ useNewUrlParser: true }, function (err, db) {  
        if(err) throw err
        var dbo = db.db("birdlance");
        photos.forEach(function (photo) {
          var myquery = { phash: photo.hash };      
          dbo.collection("blnc_imagehash").find(myquery).toArray(function(err, res) {
            if (err) throw err;
            console.log(res);
            if(res.length > 0)
            {
              if(!photo.duplicates_links)
              {
                photo.duplicates_links = '';
                res.forEach(function(phash){
                  photo.duplicates_links += phash.image_uri +','
                });
              }
              photo.duplicates_found = 1
              photo.save()
            }
            db.close();
          });
        });
      });
      res.render('account-my-photos', { user: req.user, photos, title:'Birdlance | My photos' });
    }
  );
});

// Register
router.post('/register', (req, res) => {
  const { name, email, password, password2 } = req.body;
  let errors = [];
  var parts = email.split('@');
  var answer = parts[parts.length - 1];

  if (answer == "telegmail.com" || answer == "hi2.in" || answer == "getnada.com" || answer == "vomoto.com" || answer == "zetmail.com" || answer == "crlmail.com" || answer == "abyssmail.com" || answer == "givmail.com" || answer == "crlmail.com" || answer == "boximail.com" || answer == "inboxbear.com" || answer == "tafmail.com" || answer == "robot-mail.com" || answer == "aklqo.com" || answer == "oqiwq.com" || answer == "bcaoo.com") {
    errors.push({ msg: 'This has been registered as a fake/spam account. Please be aware that creating fake accounts might result in a loss of all funds.' });
  }
  
  if (!name || !email || !password || !password2) {
    errors.push({ msg: 'Please enter all fields' });
  }

  if (password != password2) {
    errors.push({ msg: 'Passwords do not match' });
  }

  if (password.length < 6) {
    errors.push({ msg: 'Password must be at least 6 characters' });
  }

  if (errors.length > 0) {
    res.render('register', {
      errors,
      name,
      email,
      password,
      password2,
      user: req.user,
      title: 'Birdlance | Register'
    });
  } else {
    User.findOne({ email: email }).then(user => {
      if (user) {
        errors.push({ msg: 'Email already exists' });
        res.render('register', {
          errors,
          name,
          email,
          password,
          password2,
          user: req.user,
          title: 'Birdlance | Register'
        });
      } else {
        const newUser = new User({
          name,
          email,
          password
        });

        bcrypt.genSalt(10, (err, salt) => {
          const email = require('../utils/email');
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            crypto.randomBytes(20, function(err, buf) {
              var token = buf.toString('hex');
              var message = 'You received an email to confirm your account. Please verify your account by confirming your email address. Please do not forget to check your spam folder! Click <a href="/users/account-verify">here</a> if you did not receive a verification email.'
              newUser.password = hash;
              newUser.confirmEmailToken = token;
              newUser.confirmEmailExpires = Date.now() + 3600000; // 1 hour
              newUser
                .save()
                .then(user => {
                  req.flash(
                    'success_msg',
                    message
                  );
                  res.redirect('/users/login');
                  email.newUserEmail(newUser.email,newUser.name,token,req);
                })
                .catch(err => console.log(err));
            });
          });
        });
      }
    });
  }
});

// Login
router.post('/login', (req, res, next) => {
  // g-recaptcha-response is the key that browser will generate upon form submit.
  // if its blank or null means user has not selected the captcha, so return the error.
  if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
    req.flash(
      'error_msg',
      'Please complete captcha.'
      );
    return res.redirect('/users/login');
  }
  // Put your secret key here.
  var secretKey = process.env.google_reCAPTCHA;
  // req.connection.remoteAddress will provide IP address of connected user.
  var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
  // Hitting GET request to the URL, Google will respond with success or error scenario.
  request(verificationUrl,function(error,response,body) {
    body = JSON.parse(body);
    // Success will be true or false depending upon captcha validation.
    if(body.success !== undefined && !body.success) {
      req.flash(
        'error_msg',
        'captcha validation was invalid.'
        );
      return res.redirect('/users/login');
    }
  passport.authenticate('local', {
    successReturnToOrRedirect: '/users/account',
    failureRedirect: '/users/login',
    failureFlash: true
  })(req, res, next);
})});
// Logout
router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/users/login');
});

//reset password
router.post('/reset-password', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
          req.flash('error', 'No account with that email address exists.');
          return res.redirect('/users/reset-password');
        }

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

        user.save(function(err) {
          done(err, token, user);
          const forgotpasswordemail = require('../utils/email');
          forgotpasswordemail.resetPasswordEmail(user.email,user.name,token,req);
        });
      });
    },
  ], function(token,err,user) {
    req.flash('success_msg', 'An email has been sent to ' + user.email + ' with further instructions.');
    console.log('end')
    return res.redirect('/users/reset-password');
  });
});

//get reset password page to update password and check token
router.get('/password-reset/:token', function(req, res) {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/users/reset-password');
    }
    res.render('password-reset', {user: req.user, title:'Birdlance | Reset password', token: req.params.token});
  });
});

//update the password
router.post('/password-reset/:token',(req, res) => {
      User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }).then(user => {
        if (!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('back');
        }
        if(req.body.password === req.body.password2) {
          bcrypt.genSalt(10, (err, salt) => {
            const passwordresetemail = require('../utils/email');
            bcrypt.hash(req.body.password, salt, (err, hash) => {
              if (err) throw err;
              user.password = hash;
              user
                .save()
                .then(user => {
                  req.flash(
                    'success_msg',
                    'Your password has been changed.'
                  );
                  res.redirect('/users/login');
                  passwordresetemail.passwordResetEmail(user.email,user.name);
                })
                .catch(err => console.log(err));
            });
          });

        } else {
            req.flash("error", "Passwords do not match.");
            return res.redirect('back');
        }
      })
});

//create wallet
router.post("/create-wallet", ensureAuthenticated, function(req, res) {
  User.findOne({ email: req.user.email }, function(err, user) {
  var request = require('request');
  crypto.randomBytes(64, function(err, buf) {
  var key = buf.toString('hex');
  request('https://wallet.birdlance.com:9085/nxt?requestType=getAccountId&secretPhrase=' + key, function (error, response, body) {
    console.log('error:', error); // Print the error if one occurred
    console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    console.log('body:', body); // Print body
    if (error) {
      req.flash(
        'error',
        'We could not create your wallet right now. Please try again later.'
      );
      res.redirect('/users/account');
      }
      var result = JSON.parse(body);
      console.log(result.accountRS);
      console.log(result.publicKey);
      user.encryptedData = cryptr.encrypt(key);
      user.accountRS = result.accountRS;
      user.publicKey = result.publicKey;
        user
        .save()
        .then(user => {
          req.flash(
            'success_msg',
            'Your wallet has been created.'
          );
          res.redirect('/users/account');
        })
        .catch(err => console.log(err));
    })
  })
  })
});

// join airdrop
router.post("/airdrop", ensureAuthenticated, function(req, res) {
  User.findOne({ email: req.user.email }, function(err, user) {
    if (user.airdrop) {
      req.flash('error', 'You already joined the airdrop');
      return res.redirect('/users/account');
    }
    else {
      var request = require('request');
      var object = new Object();
      object.iv = req.iv;
      object.encryptedData = req.encryptedData;
      request.post({url:'https://wallet.birdlance.com:9085/nxt?', 
      form: {
        requestType: 'sendMoney',
        secretPhrase : process.env.SECRET,
        recipient : user.accountRS,
        amountNQT : 100000000000,
        feeNQT: 100000000,
        deadline: 60
      }
      }, function (err, response, body) {
      if (err) {
        req.flash(
          'error',
          'We could not send your free BLNC. Please try again later.'
        );
        res.redirect('/users/account');
      }
      console.log('error:', err); // Print the error if one occurred
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Airdrop request responded with:', body);
      user.airdrop = true;
      user
      .save()
      .then(user => {
        req.flash(
          'success_msg',
          'You joined the airdrop. You will receive your free BLNC in a few minutes.'
        );
        res.redirect('/users/account');
      })
      .catch(err => console.log(err));
    })
  }
})
});

//update account
router.post("/update-account", ensureAuthenticated, function(req, res) {
  User.findOne({ email: req.user.email }, function(err, user) {
   const { address } = req.body;
   if(address.substring(0,4) == 'BLNC') {
    user.accountRS = address;
    user.save(function(err) {
      user
      .save()
      .then(user => {
        req.flash(
          'success_msg',
          'Profile updated'
        );
        res.redirect('/users/account');
      })
      .catch(err => console.log(err));
    });
  }
  else {
    req.flash(
      'error',
      'This is not an BLNC address. BLNC is NOT a ERC20 token. You cannot use a ERC20 wallet! Create a wallet or download our desktop wallet instead.'
    );
    res.redirect('/users/account'); 
  }
  });
});

//update password
router.post("/update-password", ensureAuthenticated, function(req, res) {
  User.findOne({ email: req.user.email }, function(err, user) {
    const { currentpassword,newpassword,newpassword2 } = req.body;
    // Match password
    bcrypt.compare(currentpassword, req.user.password, (err, isMatch) => {
      if (err) throw err;
        if (isMatch) {
          if (newpassword == newpassword2) {
            bcrypt.genSalt(10, (err, salt) => {
              bcrypt.hash(newpassword, salt, (err, hash) => {
                if (err) throw err;
                user.password = hash;
                user.save(function(err) {
                  user
                  .save()
                  .then(user => {
                    req.flash(
                      'success_msg',
                      'Password succesfully updated.'
                    );
                    res.redirect('/users/account-settings');
                  })
                  .catch(err => console.log(err));
                });
              });
            });
          }
          else {
            req.flash(
              'error',
              'Passwords do not match.'
            );
            res.redirect('/users/account-settings');
          }
        } else {
          req.flash(
            'error',
            'Current password incorrect.'
          );
          res.redirect('/users/account-settings');
        }
    });
  });
});

//withdraw balance
router.post("/withdraw", ensureAuthenticated, function(req, res) {
      const { blncaddress,amount } = req.body;
      var request = require('request');
      var balance = 0;
        request('https://wallet.birdlance.com:9085/nxt?requestType=getAccount&account=' + req.user.accountRS, function (error, response, body) {
          console.log(body);
          var result = JSON.parse(body);
          balance = (result.balanceNQT / 100000000) - 1;
          if(balance >= amount && amount > 0) {
            request.post({url:'https://wallet.birdlance.com:9085/nxt?', 
            form: {
              requestType: 'sendMoney',
              secretPhrase : cryptr.decrypt(req.user.encryptedData),
              recipient : blncaddress,
              amountNQT : amount*100000000,
              feeNQT: 100000000,
              deadline: 60
            }
            }, function (err, response, body) {
            if (err) {
              req.flash(
                'error',
                'We could not withdraw your funds. Please try again later.'
              );
              res.redirect('/users/account-withdraw');
            }
            console.log('error:', err); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('Withdraw request responded with:', body);
            req.flash(
              'success_msg',
              'Your balance has been withdraw.'
            );
            res.redirect('/users/account-withdraw');
          })
        }
        else if (amount <= 0){
          req.flash(
            'error',
            'Please input a positive amount.'
          );
          res.redirect('/users/account-withdraw');
        }
        else {
          req.flash(
            'error',
            'You do not have enough BLNC to withdraw that amount. Please note you need atleast 1 BLNC as transaction fee.'
          );
          res.redirect('/users/account-withdraw');
        }
      });  
});

//confirm account
router.get('/confirm-account/:token', function(req, res) {
  User.findOne({ confirmEmailToken: req.params.token, confirmEmailExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      var message = 'The token to confirm your account is invalid or has expired. Click <a href="/users/account-verify">here</a> to request a new verification email.'
      req.flash('error', message);
      return res.redirect('/users/login');
    }
    user.accountVerified = true;
    user.save(function(err) {
      user
      .save()
      .then(user => {
        req.flash(
          'success_msg',
          'Your account has been confirmed, you can now login.'
        );
        res.redirect('/users/login');
      })
      .catch(err => console.log(err));
    });
  });
});

//reverify account
router.post('/reverify', (req, res) => {
const { email } = req.body;
User.findOne({ email: email }).then(user => {
  if (!user) {
    req.flash(
      'error',
      'That email is not registered.'
    );
    res.redirect('/users/account-verify');
  } 
  else if (user && user.accountVerified) {
    req.flash(
      'error',
      'Acccount is already verified.'
    );
    res.redirect('/users/login');
  }
  else {
      const email = require('../utils/email');
        crypto.randomBytes(20, function(err, buf) {
          var token = buf.toString('hex');
          var message = 'You received an email to confirm your account. Please verify your account by confirming your email address. Please do not forget to check your spam folder! Click <a href="/users/account-verify">here</a> if you did not receive a verification email.'
          user.confirmEmailToken = token;
          user.confirmEmailExpires = Date.now() + 3600000; // 1 hour
          user
            .save()
            .then(user => {
              req.flash(
                'success_msg',
                message
              );
              res.redirect('/users/login');
              email.newUserEmail(user.email,user.name,token,req);
            })
            .catch(err => console.log(err));
        });
    }
  })
});

//upload files
router.post("/upload", ensureAuthenticated, upload.single("file"), function(req, res) {
  const filename = crypto.randomBytes(32).toString("hex");
  const input = fs.readFileSync(req.file.path);
  var gatewayurl = "https://ipfs.infura.io/ipfs/";
  sharp(input)
  .rotate()
  .resize(400)
  .toBuffer()
  .then( data => {
      fs.writeFileSync('req.file.path', data);
      ipfs.files.add(input, function (err, hash) {
        if (err) {
          console.log(err);
        }
        console.log(hash);
        var hash = hash[0];
        var url = gatewayurl + hash.hash;
        console.log(url);

        var newFileUploaded = {
          title: req.body.title,
          tags: req.body.tags,
          fileLink: url,
          key: filename,
          created_by: req.user.email
        };

        var options = {
        uri: 'https://birdlance-api-image-hash.herokuapp.com/api_v1.0/calculate',
        method: 'POST',
          json: {
            "identifier_id": filename,
            "identifier_type":"Birdlance-image-hash API",
            "image": {
              "id":filename,
              "url":url
            }
          }
        }
        var document = new DOCUMENT(newFileUploaded);
        document.save(function(errorsave, newFile) {
          if (errorsave) {
            throw errorsave;
          }
          if (newFile) {
            res.redirect('/users/account')
          }
        });
        request(options, function (error, response, api_body) {
          if(error) {
            console.log(error);
          }
          if(response) {
            console.log("Birdlance-image-hash API: "+ response.statusCode) // Print api response.
          }
        });
      });
      
      fs.unlink(req.file.path);
    });
});
module.exports = router;
