// emails.js
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY) // Input Api key or add to environment config
function newUserEmail(email, name, token, req){
    const message = { 
      to : email, //email variable
      from : { email : 'noreply@birdlance.com' , name: 'Birdlance'},
      templateId: 'd-7d28f350c52b480396d4ea3b573da0b5',
      dynamic_template_data: {
        name: name,
        url: 'https://' + req.headers.host + '/users/confirm-account/' + token,
      },
    };
    sgMail.send(message).then((sent) => {
      //
    })
    .catch(error => {

        //Log friendly error
        console.error(error.toString());
    
        //Extract error msg
        const {message, code, response} = error;
    
        //Extract response msg
        const {headers, body} = response;
      });
   }

   function resetPasswordEmail(email, name, token,req){
    const message = { 
      to : email, //email variable
      from : { email : 'noreply@birdlance.com' , name: 'Birdlance'},
      templateId: 'd-813f4fafce284ef18da1d4e242ab290a',
      dynamic_template_data: {
        name: name,
        url: 'https://' + req.headers.host + '/users/password-reset/' + token,
      },
    };
    sgMail.send(message).then((sent) => {
      // Awesome Logic to check if mail was sent
    })
    .catch(error => {

        //Log friendly error
        console.error(error.toString());
    
        //Extract error msg
        const {message, code, response} = error;
    
        //Extract response msg
        const {headers, body} = response;
      });
   }

   function passwordResetEmail(email, name){
    const message = { 
    to : email, //email variable
    from : { email : 'noreply@birdlance.com' , name: 'Birdlance'},
    templateId: 'd-e4506f7b49fc4796a08a0f0601eb22ce',
    dynamic_template_data: {
      name: name,
    },
  };
    sgMail.send(message).then((sent) => {
      // Awesome Logic to check if mail was sent
    })
    .catch(error => {

        //Log friendly error
        console.error(error.toString());
    
        //Extract error msg
        const {message, code, response} = error;
    
        //Extract response msg
        const {headers, body} = response;
      });
   }
   module.exports = {
    newUserEmail,
    resetPasswordEmail,
    passwordResetEmail
   }