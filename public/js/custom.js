$(function() {

  "use strict";

  $('.shot-modal-opener, .shot-preview .img, .shot-preview .black-overlay, .shot-preview .title, .shot-preview .text-overlay, .shot-small .shot-info h6 a').on('click', function(e) {
    
    e.preventDefault();

    // Put a spinner inside the modal and open it
    $('#shot-modal .modal-content').html('<div class="spinner"><span class="dot1"></span><span class="dot2"></span><span class="dot3"></span></div>');
    $('#shot-modal').modal();
    var myImageId = $(this).data('id');
    var title =  $(this).data('title');
    var transaction = $(this).data('transaction');
    if(transaction){
      transaction = '<a href="' + transaction + '" target="_blank">' + transaction + '</a>'
    }
    var tags = $(this).data('tags');
    var location  = $(this).data('location');

    $('#shot-modal .modal-content').load('/shot', function() {
      // Get the button that opens the duplicates modal
      var btn = document.getElementById("duplicatesBtn");
      if(location) {
        var location_array = location.split(",")
        var lst = "<ul>"
        location_array.forEach(locationuri => {
          lst += '<li>' + '<a href="' + locationuri + '" target="_blank">' + locationuri + '</a>' + '</li>'
        });
        lst +="</ul>"
        // Get the modal for duplicates
        var modal = document.getElementById("duplicatesModal");
        // Get the <span> element that closes the duplicates modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on the button, open the duplicates modal
        btn.onclick = function() {
          modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the duplicates modal
        span.onclick = function() {
          modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
          if (event.target == modal) {
            modal.style.display = "none";
          }
        }
      }
      else {
          // set button hidden
          btn.style.visibility = 'hidden';
      }
      $('.modal-content-img #myImage').attr("src", myImageId);  
      
      $('.image-info #image-title').html(title);  
      $('.image-info #image-transaction').html(transaction);  
      
      $('.image-upload-tags .text').html(tags); 
      $('.image-upload-location .text').html(lst);  
    });

  });
});

$(function() {
  $('#fileForm').bootstrapValidator({
      feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          file: {
              validators: {
                  file: {
                      extension: 'jpeg,png,jpg',
                      type: 'image/jpeg,image/png,image/jpg',
                      message: 'The selected file is not valid'
                  }
              }
          }
      }
  });
});

