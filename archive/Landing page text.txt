Website
=======
-\ Landing page
MH--\ Link to wallet installer
MH--\ Link to white paper
OK--\ Information about the Birdlance project
OK--\ Information about the airdrop
OK--\ Registration form for receiving airdrop rewards
--\ Additional purchase of tokens ($0,008/Token)
---\ Vergelijking maken tussen het aantal tokens en de waarde in BTC (1; 0,5; 0,1; 0,01)
---\ Enkel te betalen met eigen crypto coins
---\ Versturen naar een ETH/BTC adres
OK-\ Telegram group maken
OK-\ Twitter
OK-\ Facebook
-\ Instagram (?)
OK-\ Contact form when an issue has occurred.
-- Install file, zoeken waarom NXT wordt gebruikt

======
+=======================================+
|Information about the Birdlance project|
+=======================================+
Birdlance provides a water proof way of proving the authorship of a photograph by combining the immutability of
a Blockchain together with state of the art image recognition and image similarity techniques. Unsolicited usage
of a photograph in either its original as in its edited form, can be detected by Birdlance and will be reported
to the original author of the content.

On the Birdlance platform Photographers and Graphic designers can upload their content and make them publicly available 
for sale to other users. Once the content has been uploaded, Birdlance will apply various image recognition and image 
similarity techniques to identify the authenticity of the content and act appropriately regarding the result. When
accepted, the content will automatically be classified in certain categories based on the visual aspects of the content.
Users can buy content on the Birdlance platform using Birdlance Coins (BLC). 

We at Birdlance eventually wish to become an authority for proving the authenticity of imagery content and link it to their 
original creator.

+==========================================+
|Information about the Birdlance Coin (BLC)|
+==========================================+
Birdlance Coin (BLC)
--------------------
Birdlance offers its own utility token BLC as a currency for buying and selling photographs on the platform. Initially the 
value will be set at $0,008/BLC but will change over time depending on the general supply and demand by the crypto community.

  $1 ~ 0,00009BTC ~ 0,003222ETH ~ 0.007411LTC = 125BLC
 $10 ~  0,0009BTC ~  0,03222ETH ~  0.07411LTC = 1,250BLC
$100 ~   0,009BTC ~   0,3222ETH ~   0.7411LTC = 12,250BLC 

   1BTC ~ $11,059.426724 ~ 1,382,428.3405BLC
 0.5BTC ~ $ 5,529.713362 ~   691,214.1703BLC
 0.1BTC ~ $ 1,105.942672 ~   138,242.8341BLC
0.01BTC ~ $   110.594267 ~    13,824.2834BLC

Free BLC airdrop
----------------
At first, Birdlance will make a total of 10 million BLC available for free and this for a limited time or until all BLC have
been distributed. Each newly registered Birdlance user will receive a one time only airdrop of 1,000 BLC to kick-start their 
Birdlance experience. To apply for the Birdlance Coin airdrop, please fill in the registration form and receive your part of 
the free BLCs. As soon as the Birdlance platform goes live, you will receive all the information necessary to join the 
Birdlance family and the possibility to create an early-bird user account.

Buy extra BLC
-------------
Have you applied for the free BLC airdrop, but are 1,000 BLC not enough? We have got you covered! You can easily buy additional
BLC by sending BTC/ETH/LTC to our corresponding addresses. Use the following converter to convert your BTC/ETH/LTC to BLC.

+========================+
|Link to wallet installer|
+========================+
Lightweight wallet
------------------
Birdlance is proud to announce that it runs on its own Proof-of-Stake Blockchain. We have our own lightweight wallet to store 
and keep your BLCs safe. Feel free to download and install the wallet here.

Heavyweight wallet
------------------
Are you a more tech experienced person and are you willing to contribute to the Birdlance network by running your own node, 
which in turn will make you receive extra rewards by staking BLCs using our wallet? 
Feel free to download and install the wallet here.

+============+
|Contact form|
+============+
After reading our landing page do you still have some questions unanswered regarding Birdlance, the Birdlance airdrop or the Birdlance wallet?
Please do not hesitate to contact us by filling in our contact form and we will do our best that all your questions will get answered!

+===========================+
|Stay connected to Birdlance|
+===========================+
Do you want to stay updated on the progress of Birdlance? Join us on Facebook, Twitter and Telegram.

+=====================+
|Buy us a coffee, or 2|
+=====================+
The Birdlance team is still working hard to bring the entire Birdlance experience to you as fast as possible. Do you like
what we are doing and are you willing to extra motivate us? We would highly appreciate you buing us a nice cup of coffee to 
fuel our engine to speed up the process.

