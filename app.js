const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const path = require('path');
const app = express();
const MongoStore = require('connect-mongo')(session);
require('dotenv').config();

const rateLimit = require("express-rate-limit");
 
// Enable if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
// see https://expressjs.com/en/guide/behind-proxies.html
//app.set('trust proxy', 1);
 
//const limiter = rateLimit({
//  windowMs: 15 * 60 * 1000, // 15 minutes
//  max: 10 // limit each IP to 100 requests per windowMs
//});
 
//  apply to all requests
//app.use(limiter);

// Passport Config
require('./config/passport')(passport);

// DB COnfig
const db = process.env.MongoURI;

// Connect to Mongo
mongoose.connect(db, { useNewUrlParser: true})
.then(() => console.log('MongoDB connected...'))
.catch(err => console.log(err));

//EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

// set path for static assets
app.use(express.static(path.join(__dirname, 'public')));
app.use('/users',express.static(__dirname + '/public'));
app.use('/users/settings',express.static(__dirname + '/public'));
app.use('/users/:token',express.static(__dirname + '/public'));

// Express body parser
app.use(express.urlencoded({ extended: true }));

//session
app.use(session({
  store: new MongoStore({
    mongooseConnection: mongoose.connection
  }),
      secret: process.env.SESSION_SECRET,
      resave: false,
      saveUninitialized: false,
      cookie: {
          maxAge: 1000 * 60 * 60 * 24 * 7 * 2 // two weeks
    }
}));
  
// Passport middleware
app.use(passport.initialize());
app.use(passport.session());
  
// Connect flash
app.use(flash());
  
// Global variables
app.use(function(req, res, next) {
res.locals.success_msg = req.flash('success_msg');
res.locals.error_msg = req.flash('error_msg');
res.locals.error = req.flash('error');
next();
});

//Routes
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));

const PORT = process.env.PORT || 5000;

app.listen(PORT,console.log(`Server started on port ${PORT}`));